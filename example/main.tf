module "backend" {
  source = "../"

  name            = null
  code_dir        = null
  code_entrypoint = null
  code_runtime    = null
  api_version     = null

  api_methods = {
    message = ["GET"],
    date    = ["GET"],
    secret  = ["POST"]
  }
}