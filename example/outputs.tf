output "methods_urls" {
  value = module.backend.methods_urls
}

output "origin" {
  value = module.backend.origin
}