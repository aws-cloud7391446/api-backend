import json

from methods import method_message, method_date, method_secret


def lambda_proxy_return(status_code = 200, body = ''):
    if type(body) != str:
        body = json.dumps(body)

    return {
        'statusCode': status_code,
        'body': body
    }

def lambda_handler(event, context):
    method = None
    response = None
    code = 200

    if event.get('httpMethod') is not None:
        method = event.get('path').split('/')[3]
        response = {
            'httpMethod':   event.get('httpMethod'),
            'apiVersion':   event.get('path').split('/')[2],
            'method':       method
        }
    else:
        response = {
            'info': 'Not a HTTP request'
        }
    
    if method == 'message':
        code, response = method_message(response)
    elif method == 'date':
        code, response = method_date(response)
    elif method == 'secret':
        code, response = method_secret(response, event.get('body'))


    return lambda_proxy_return(code, response)