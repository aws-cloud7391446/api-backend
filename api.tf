resource "aws_api_gateway_rest_api" "api" {
  name = var.name
}

resource "aws_api_gateway_resource" "api" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "api"
}
resource "aws_api_gateway_resource" "api_version" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_resource.api.id
  path_part   = var.api_version
}


resource "aws_api_gateway_resource" "api_resources" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_resource.api_version.id

  for_each    = local.api_methods_resources
  path_part   = each.value
}

resource "aws_api_gateway_method" "api_methods" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  authorization = "NONE"

  for_each      = local.api_methods
  resource_id   = aws_api_gateway_resource.api_resources[each.value.name].id
  http_method   = each.value.http_method
}

resource "time_sleep" "api_methods_delay" {
  depends_on = [aws_api_gateway_resource.api_resources]

  create_duration = "5s"
}

resource "aws_api_gateway_integration" "api_integrations" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda.invoke_arn

  for_each                = local.api_methods
  resource_id             = aws_api_gateway_resource.api_resources[each.value.name].id
  http_method             = each.value.http_method

  depends_on              = [time_sleep.api_methods_delay]
}