data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "lambda_role" {
  name               = "${var.name}-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}


data "archive_file" "lambda" {
  type        = "zip"
  source_dir  = "${path.root}/${var.code_dir}"
  output_path = "${path.root}/${var.code_dir}.zip"
}

resource "aws_lambda_function" "lambda" {
  filename         = "${path.root}/${var.code_dir}.zip"
  function_name    = "${var.name}-lambda-function"
  role             = aws_iam_role.lambda_role.arn
  handler          = var.code_entrypoint
  runtime          = var.code_runtime
  source_code_hash = filebase64sha256("${path.root}/${var.code_dir}.zip")

  depends_on       = [data.archive_file.lambda]
}