output "methods_urls" {
  value = toset([
    for method_id, method in local.api_methods : "${aws_api_gateway_stage.prod.invoke_url}/api/${var.api_version}/${method.name}"
  ])
}

output "origin" {
  value = split("://", aws_api_gateway_stage.prod.invoke_url)[1]
}