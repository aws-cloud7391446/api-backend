# API Backend
Allows you to quickly deploy a serverless backend using an API Gateway with a Lambda function
using methods path of this structure - `/api/<api_version>/<name>`

## Architecture
![Schema](/uploads/c10579f05826d7d86696787090ac95c5/schema.png)

Code is stored in the Lambda Function which is then called and executed by the API Gateway when a request is accepted  
Path for methods is `/api/<api_version>/<name>` where `<api_version>` is a version of a REST API, for example 'v1' or 'v2'  
Methods names have a limitation and must be called straightforward without subpaths!
Also there's no support for methods like `/api/v1/method/<argument>`!

## Managed Resources
List of AWS resources that're managed by this terraform application

- API Gateway REST API
- API Gateway Resources
- API Gateway Methods
- API Gateway Intergrations
- IAM Role
- Lambda Function